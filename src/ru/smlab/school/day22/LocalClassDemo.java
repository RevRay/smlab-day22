package ru.smlab.school.day22;

public class LocalClassDemo {
    public static void main(String[] args) {
        int x = 6;


        class LocalClass {
            int i = 9;
            int j = 9;

            public LocalClass(int i, int j) {
                this.i = i;
                this.j = j;
            }

            @Override
            public String toString() {
                return "LocalClass{" +
                        "i=" + i +
                        ", j=" + j +
                        '}';
            }
        }

        LocalClass l = new LocalClass(2,3);
        System.out.println(l);
    }

    public static void demo() {

    }

}
