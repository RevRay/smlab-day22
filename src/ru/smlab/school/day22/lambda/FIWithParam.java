package ru.smlab.school.day22.lambda;

@FunctionalInterface
public interface FIWithParam {
    int count(int a, int b);
}
