package ru.smlab.school.day22.lambda;

//Функциональный интерфейс (functional interface) – это интерфейс у которого только один абстрактный метод.
@FunctionalInterface
public interface ExampleFunctionalInterface {
    void doStuff();

    default void doStuff1(){
        System.out.println();
    }

    static void check(){

    }
}
