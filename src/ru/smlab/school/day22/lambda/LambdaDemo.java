package ru.smlab.school.day22.lambda;

import java.util.List;

public class LambdaDemo {
    //(переменные) -> действие
    public static void main(String[] args) {
        ExampleFunctionalInterface e = () -> System.out.println("123");
        e.doStuff();

        FIWithParam fi = (a1, a2) -> {
            int result = a1 + a2;
            System.out.println(result);
            return result;
        };

        int result = fi.count(4,5);
    }
}
