package ru.smlab.school.day22;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    static class Child {
        int i = 0;
    }

    class NonStaticChild {

    }

    public static void main(String[] args) {
        Main topLevelClass = new Main();
        System.out.println(topLevelClass.getClass());

        Main.Child staticNestedClass = new Main.Child();
        System.out.println(staticNestedClass.getClass());




        Main m = new Main();
        NonStaticChild n = m.new NonStaticChild();
        System.out.println(n.getClass());

        class LocalClass {}
        LocalClass l = new LocalClass();
        System.out.println(l.getClass());

    }

}
class Another {

}
