package ru.smlab.school.day22.getter;

public class BankCard2 {
    private final CardNumber cardNumber;
    private int balance;

    public BankCard2(CardNumber cardNumber, int balance) {
        this.cardNumber = cardNumber;
        this.balance = balance;
    }

    public CardNumber getCardNumber() {
        return new CardNumber(this.cardNumber.cardNumber);
    }

//    private void setCardNumber(CardNumber cardNumber) {
//        this.cardNumber = cardNumber;
//    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BankCard{" +
                "cardNumber='" + cardNumber + '\'' +
                ", balance=" + balance +
                '}';
    }
}
