package ru.smlab.school.day22.getter;

public class CardDemo {
    public static void main(String[] args) {
        BankCard b = new BankCard("000-999asf", 90);
        b.getCardNumber().toUpperCase();

        System.out.println(b);


        BankCard2 b2 = new BankCard2(new CardNumber("4545-4354fdgsgdf"), 50);
        CardNumber cn = b2.getCardNumber();
//        cn.setCardNumber("fsdfsdf");

        System.out.println(b2);
    }
}
