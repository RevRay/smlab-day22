package ru.smlab.school.day22.getter;

public class CardNumber {
    String cardNumber;

    public CardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    private String getCardNumber() {
        return cardNumber;
    }

    private void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public String toString() {
        return "CardNumber{" +
                "cardNumber='" + cardNumber + '\'' +
                '}';
    }
}
