package ru.smlab.school.day22.getter;

import java.util.HashMap;

public class Fruit {

    public Fruit(int weight, String name) {
        if (weight < 0) throw new RuntimeException();
        this.weight = weight;
        this.name = name;
    }

    private int weight;
    private String name;

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Fruit{" +
                "weight=" + weight +
                ", name='" + name + '\'' +
                '}';
    }
}
