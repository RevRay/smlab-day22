package ru.smlab.school.day22.getter;

public class FruitLauncher {
    public static void main(String[] args) {
        Fruit f = new Fruit(2, "banana");
        System.out.println(f);

        f.getWeight();
        f.setWeight(20);
        f.setWeight(19);
        f.setWeight(18);

        System.out.println(f);
    }
}
