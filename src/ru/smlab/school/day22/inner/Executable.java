package ru.smlab.school.day22.inner;

import java.util.List;

public interface Executable {
    void execute(List<String> list, int i);
}
