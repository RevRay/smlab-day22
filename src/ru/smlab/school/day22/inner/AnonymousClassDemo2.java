package ru.smlab.school.day22.inner;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AnonymousClassDemo2 {
    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>();
        list1.add("check");
        list1.add("these");
        list1.add("values");
        list1.add("values");
        list1.add("values");
        list1.add("values");

        List<String> list2 = new LinkedList<>();
        list2.add("check");
        list2.add("these");
        list2.add("values");
        list2.add("values");
        list2.add("values");
        list2.add("values");

        Executable addOperation = (list, i) -> list.add("fsdfsdfsd" + i);
//        Executable addOperation = new Executable() {
//            @Override
//            public void execute(List<String> list, int i) {
//                list.add("fsdfsdfsd" + i);
//            }
//        };

//        Executable insertOperation = new Executable() {
//            @Override
//            public void execute(List<String> list, int i) {
//                list.add(4, "fsdfs" + i);
//            }
//        };

        Executable insertOperation = new InsertOperation();

        measureOperation(list1, addOperation);
        measureOperation(list2, addOperation);
        measureOperation(list1, insertOperation);
        measureOperation(list2, insertOperation);
    }

    public static void measureOperation(List<String> list, Executable executable){
        long beforeStart = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            executable.execute(list, i);
//            list.add("asfdfsd" + i); //как параметризовать выполняемый код
        }
        long afterStart = System.currentTimeMillis();
        System.out.println("add - time elapsed: " + (afterStart - beforeStart));
    }


}
