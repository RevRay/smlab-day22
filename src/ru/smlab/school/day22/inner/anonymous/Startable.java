package ru.smlab.school.day22.inner.anonymous;

public interface Startable {
    void start();
}
