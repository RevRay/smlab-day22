package ru.smlab.school.day22.inner.anonymous;

public interface Stoppable {
    void stop();
}
