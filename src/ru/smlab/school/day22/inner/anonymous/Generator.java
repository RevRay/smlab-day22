package ru.smlab.school.day22.inner.anonymous;

public abstract class Generator implements Startable, Stoppable{

    @Override
    public void start() {
        System.out.println("Default ignition");
    }

}
