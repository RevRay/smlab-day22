package ru.smlab.school.day22.inner;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AnonymousClassDemo {
    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>();
        list1.add("check");
        list1.add("these");
        list1.add("values");
        list1.add("values");
        list1.add("values");
        list1.add("values");

        List<String> list2 = new LinkedList<>();
        list2.add("check");
        list2.add("these");
        list2.add("values");
        list2.add("values");
        list2.add("values");
        list2.add("values");

        measureAddOperation(list1);
        measureAddOperation(list2);
    }

    public static void measureAddOperation(List<String> list){
        long beforeStart = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            list.add("asfdfsd" + i); //как параметризовать выполняемый код
        }
        long afterStart = System.currentTimeMillis();
        System.out.println("add - time elapsed: " + (afterStart - beforeStart));
    }

    public static void measureRemoveOperation(List<String> list){
        long beforeStart = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            list.remove("asfdfsd" + i);
        }
        long afterStart = System.currentTimeMillis();
        System.out.println("remove - time elapsed: " + (afterStart - beforeStart));
    }

    public static void measureInsertOperation(List<String> list){
        long beforeStart = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            list.add(2,"asfdfsd" + i);
        }
        long afterStart = System.currentTimeMillis();
        System.out.println("insert - time elapsed: " + (afterStart - beforeStart));
    }

    public static void measureGetOperation(List<String> list){
        long beforeStart = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            Object o = list.get(5);
        }
        long afterStart = System.currentTimeMillis();
        System.out.println("get - time elapsed: " + (afterStart - beforeStart));
    }
}
