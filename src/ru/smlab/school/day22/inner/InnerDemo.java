package ru.smlab.school.day22.inner;

import ru.smlab.school.day22.inner.anonymous.DieselGenerator;
import ru.smlab.school.day22.inner.anonymous.SmartDieselGenerator;
import ru.smlab.school.day22.inner.anonymous.Startable;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class InnerDemo {
    public static void main(String[] args) {
        DieselGenerator d = new DieselGenerator();

//        SmartDieselGenerator sm = new SmartDieselGenerator();
//        sm.start();

        DieselGenerator d1 = new DieselGenerator(){
            int i = 90;

            @Override
            public void start() {
                System.out.println("ignition started");
                super.start();
            }
        };

        d1.start();
        System.out.println(d1.getClass());


        Startable s1 = new Startable() {
            @Override
            public void start() {
                System.out.println("start");
            }
        };
        Startable s2 = new Startable() {
            @Override
            public void start() {
                System.out.println("start");
            }
        };
        System.out.println(s1.getClass());
        System.out.println(s2.getClass());
        System.out.println(s1.getClass().equals(s2.getClass()));

    }
}
